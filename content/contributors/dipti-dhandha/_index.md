---
title: Dipti Dhandha
image: 
    url: https://payatu.com/static/images/members/DIPTI.png
    alt: https://payatu.com/static/images/members/DIPTI.png
socials:
    linkedin: https://www.linkedin.com/in/dipti-dhandha-87847a138
    twitter: https://twitter.com/Dipti_Dhandha
    github: https://github.com/DiptiKhatri
draft: false
# Do not change the below values.
type: contributors
layout: single
---

Dipti is a Security Consultant at Payatu. She is specialised in performing pentesting on Web Application and Mobile Application (Android).
